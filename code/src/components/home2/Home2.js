import React, { Component } from 'react';
import Profile from './../Profile/Profile';
import BoxContainer from './../BoxContainer/BoxContainer';
import Skills from './../Skills/Skills';
import {Container, Row, Col} from 'react-bootstrap';
import Pittsburgh from '../../images/pittsburgh2.jpg';
import data from '../data.json';
import './Home2.scss';

export default class Home2 extends Component {

    constructor() {
        super();
    }

    render() {
        return (
            <Container className='home2'>
                <Row>
                    <Col  xs={12}>
                        <img src={Pittsburgh} className='backgroundPhoto'/>
                        <Container className='profileContainer'>
                            <Row>
                                <Col xs={3} />
                                <Col xs={6} >
                                    <Container>
                                        <Row>
                                            <Col>
                                                <Profile/>
                                            </Col>
                                        </Row>
                                        <Row className='mt-4'>
                                            <Col  xs={12} >
                                                <BoxContainer title='About Me' >
                                                    {data.AboutMe}
                                                </BoxContainer>
                                            </Col>
                                        </Row>
                                        <Row className='mt-4'>
                                            <Col  xs={12} >
                                                <Skills />
                                            </Col>
                                        </Row>
                                    </Container>
                                </Col>
                                <Col xs={3} />
                            </Row>
                        </Container>
                    </Col>
                </Row>
            </Container>
        );
    }
}