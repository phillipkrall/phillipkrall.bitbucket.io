import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from 'react-router-dom';
import Home from './home/Home';
import Home2 from './home2/Home2';
export default class App extends Component {

    constructor() {
        super();
    }

    render() {
        return (
            <Router>
                <div>
                    <nav>
                        <ul>
                            <li>
                                <Link to="/">Home</Link>
                            </li>
                            <li>
                                <Link to="/home2">Home2</Link>
                            </li>
                        </ul>
                    </nav>

                    {/* A <Switch> looks through its children <Route>s and
                        renders the first one that matches the current URL. */}
                    <Switch>
                        <Route path="/home2">
                            <div className='container'>                
                                <Home />
                            </div>
                        </Route>
                        <Route path="/">
                            <div className='container'>                
                                <Home2 />
                            </div>
                        </Route>
                    </Switch>
                </div>
            </Router>
            
        );
    }
}