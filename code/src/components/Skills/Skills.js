import React, { Component } from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import data from '../data.json';

export default class Skills extends Component {

    constructor() {
        super();
    }

    getSkillCard(skill) {
        let talents = skill.talent.map((name) => 
            <li  key="{idx}">{name}</li>
        );
        return (
            <div className="bg-dark p-3">
                <h5 className="text-light">{skill.name}</h5>
                <hr className='bar'/>
                <ul className="text-muted">
                    {talents}
                </ul>
            </div>
        );
    }

    render() {
        let skills = data.skills.map((skill) =>
            <Col xs={3} className='mr-3' key="{idx}">{this.getSkillCard(skill)}</Col>
        );
        return (
            <div>
                <h1 className='text-light'>Skills</h1> 
                <Container>
                    <Row>
                        {skills}
                    </Row>
                </Container>
            </div>
        );
    }
}