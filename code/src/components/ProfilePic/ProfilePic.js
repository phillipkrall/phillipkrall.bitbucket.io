import React, { Component } from 'react';
import personPhoto from '../../images/phill.jpg';
import './ProfilePic.scss';

export default class ProfilePic extends Component {

    constructor() {
        super();
    }

    render() {
        return (
            <div className='personPhotoContainer'>                
                <img src={personPhoto} />
            </div>
        );
    }
}