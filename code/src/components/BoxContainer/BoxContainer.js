import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class BoxContainer extends Component {

    static get propTypes() { 
        return { 
            title: PropTypes.any, 
            children: PropTypes.any 
        }; 
    }


    constructor() {
        super();
    }
    render() {
        return (
            <div>
                { this.props.title != '' && <h1 className='text-light'>{this.props.title}</h1> }
                <div className='pt-3 pl-3 pb-3 pr-3 bg-dark'>
                    <p className='text-muted'  dangerouslySetInnerHTML={{ __html: this.props.children }}></p>
                </div>
            </div>
        );
    }
}