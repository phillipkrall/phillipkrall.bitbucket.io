import React, { Component } from 'react';
import ProfilePic from './../ProfilePic/ProfilePic';
import {Container, Row, Col, Button} from 'react-bootstrap';
import GitHub from '../../images/github.svg';
import LinkedIn from '../../images/linkedin.svg';
import Facebook from '../../images/facebook.svg';
import BitBucket from '../../images/bitbucket.svg';
import Calendar from '../../images/calendar.svg';
import './Profile.scss';

export default class Profile extends Component {

    constructor() {
        super();
    }

    age() {
        var diff_ms = Date.now() - new Date(1993,10,6).getTime();
        var age_dt = new Date(diff_ms); 
        return Math.abs(age_dt.getUTCFullYear() - 1970);
    }

    render() {
        return (
            <div xs={6} className='profile  bg-dark'>
                <Container>
                    <Row>
                        <Col xs={5} className='pl-3 pt-3'>
                            <ProfilePic  />
                            <h6 className='pl-4 mt-3'>
                                <a href="https://github.com/pkrall520"><img className='profileIcon' src={GitHub}/></a>
                                <a href="https://bitbucket.org/%7Bb9a544c6-3469-43ac-bf71-2a6a76517965%7D/"><img className='profileIcon' src={BitBucket}/></a>
                                <a href="https://www.linkedin.com/in/phillipkrall"><img className='profileIcon' src={LinkedIn}/></a>
                                <a href="https://www.facebook.com/phillip.krall.1"><img className='profileIcon' src={Facebook}/></a>
                            </h6>
                        </Col>
                        <Col className=' pt-3 pb-4'>
                            <h1 className=" text-light">Phillip Krall</h1>
                            <h4 className="text-muted ">Software Architect & Manager</h4>
                            <Container className='mt-4'>
                                <Row>
                                    <Col className="pl-0"  xs={5}>
                                        <Button variant="outline-success" className="rounded-0">Download Resume</Button> 
                                    </Col>
                                    <Col>
                                        <Button variant="outline-primary" className=" rounded-0">Contact Me!</Button>
                                    </Col>
                                </Row>
                                <Row className='mt-4'>
                                    <Col className="pl-0 text-light"  xs={5}>
                                        <small>Age: </small>
                                    </Col>
                                    <Col>
                                        <small className="text-muted  font-weight-light">{this.age()}</small>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col className="pl-0  text-light" xs={5}>
                                        <small>Home Town: </small>
                                    </Col>
                                    <Col>
                                        <small className="text-muted  font-weight-light">Pittsburgh, Pennsylvania</small>
                                    </Col>
                                </Row>
                                <Row >
                                    <Col className="pl-0 text-light"  xs={5}>
                                        <small>Email: </small>
                                    </Col>
                                    <Col>
                                        <small><a href="mailto:phllpkrall520@gmail.com" className="text-muted  font-weight-light">phllpkrall520@gmail.com</a></small>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col className="pl-0  text-light" xs={5}>
                                        <small>Phone: </small>
                                    </Col>
                                    <Col>
                                        <small className="text-muted  font-weight-light">740.260.3075</small>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col className="pl-0  text-light" xs={5}>
                                        <small>Calendar: </small>
                                    </Col>
                                    <Col>
                                        <small><a href="https://calendar.google.com/calendar/r/week" className="text-muted  font-weight-light">Available <img className='calendar' src={Calendar}/></a></small>
                                    </Col>
                                </Row>
                            </Container>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}