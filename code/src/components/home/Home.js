import React, { Component } from 'react';
import ProfilePic from './../ProfilePic/ProfilePic';
import {Container, Row, Col} from 'react-bootstrap';
import Pittsburgh from '../../images/pittsburgh.jpg';
import GitHub from '../../images/github.svg';
import LinkedIn from '../../images/linkedin.svg';
import Facebook from '../../images/facebook.svg';
import BitBucket from '../../images/bitbucket.svg';
import './Home.scss';

export default class Home extends Component {

    constructor() {
        super();
    }

    render() {
        return (
            <Container className='home'>
                <Row>
                    <Col  xs={12}>
                        <img src={Pittsburgh} className='backgroundPhoto'/>
                        <div className='backgroundPhotoClipLeft' />
                        <div className='backgroundPhotoClipRight' />
                        
                        <ProfilePic className='profilePic' />
                        <h6>
                            <a href="https://github.com/pkrall520"><img className='profileIcon' src={GitHub}/></a>
                            <a href="https://bitbucket.org/%7Bb9a544c6-3469-43ac-bf71-2a6a76517965%7D/"><img className='profileIcon' src={BitBucket}/></a>
                            <a href="https://www.linkedin.com/in/phillipkrall"><img className='profileIcon' src={LinkedIn}/></a>
                            <a href="https://www.facebook.com/phillip.krall.1"><img className='profileIcon' src={Facebook}/></a>
                        </h6>
                    </Col>
                </Row>
                <Row>
                    <Col xs={3}></Col>
                    <Col xs={6} className='profileName'>
                        <h1>Phillip Krall (Phill)</h1>
                        <h6 className="profileTitle">Pittsburgh, Pennsylvania</h6>
                        <h6 className="profileTitle">Programmer Guru</h6>
                        
                    </Col>
                    <Col xs={3}></Col>
                </Row>
            </Container>
        );
    }
}